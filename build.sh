#!/bin/sh

npm run build -- contents::iosevka-plex --jCmd=$(nproc)
npm run build -- contents::iosevka-plex-aile --jCmd=$(nproc)
npm run build -- contents::iosevka-plex-etoile --jCmd=$(nproc)
npm run build -- contents::iosevka-plex-haskell --jCmd=$(nproc)

npm run build -- ttf::iosevka-plex-term --jCmd=$(nproc)
npm run build -- ttf::iosevka-plex-ml --jCmd=$(nproc)
npm run build -- ttf::iosevka-plex-js --jCmd=$(nproc)
